const path = require('path');

module.exports = ({ config }) => {
  config.resolve.alias = {
    ...config.resolve.alias,
    components: path.resolve(__dirname, '../src/components'),
    ui: path.resolve(__dirname, '../src/ui'),
    app: path.resolve(__dirname, '../src/app'),
    utils: path.resolve(__dirname, '../src/utils')
  };
  // Return the altered config
  return config;
};
