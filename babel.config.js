const presets = [
  [
    '@babel/preset-env',
    {
      targets: ['last 2 versions', 'ie >= 11']
    }
  ]
];

const plugins = [
  '@babel/plugin-transform-async-to-generator',
  '@babel/plugin-proposal-object-rest-spread'
];

module.exports = { presets, plugins };
