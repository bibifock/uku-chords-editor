#!/usr/bin/env node

const yargs = require('yargs');

const migrateCmd = require('./migrate');

const registerCommand = ({ usage, description, definition, action }) =>
  yargs.command(usage, description, definition, action);

registerCommand(migrateCmd);

/* eslint-disable-next-line no-unused-expressions */
yargs
  .help()
  .argv
