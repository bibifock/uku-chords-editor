var sqlite = require('sqlite');

const action = () => {
  sqlite.open(process.env.DB_SOURCE)
    .then(db => db.migrate({ force: 'last' }));
};

const usage = 'migrate';
const description = 'play migration in database .env.DB_SOURCE';
const definition = _ => {};

module.exports = {
  action,
  usage,
  definition,
  description
};
