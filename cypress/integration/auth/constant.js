export const URL_LOGIN = '/auth/login';
export const URL_REGISTER = '/auth/register';
export const API_URL_REGISTER = `/api${URL_REGISTER}.json`;
export const API_URL_LOGIN = `/api${URL_LOGIN}.json`;
export const API_URL_LOGOUT = '/api/auth/logout.json';
