import { URL_LOGIN } from './constant';

describe('auth/login', () => {
  const fillForm = () => {
    cy.get('input[type="text"]').type('bibi@bibi.bi');
    cy.get('input[type="password"]').type('01234567890');
  };

  beforeEach(() => {
    cy.visitApp(URL_LOGIN);
  });

  it('should render correctly', () => {
    cy.get('input[type="text"]').should('exist');
    cy.get('input[type="password"]').should('exist');
    cy.get('button').should('exist')
  });

  it('should display error', () => {
    fillForm();
    cy.server();
    cy.route({
      method: 'POST',
      url: '/api/auth/login',
      response: { errors: { login: 'iups this is not working' } }
    });

    cy.get('button').click();

    cy.get('ul.alert').should('exist');
  });

  it('should redirect to home on success login', () => {
    cy.login();

    cy.url().should('match', /^https?:\/\/[^/]+\/?$/);
  });
});
