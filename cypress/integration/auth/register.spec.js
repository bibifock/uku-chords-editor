import { API_URL_REGISTER, URL_LOGIN } from './constant';

describe('auth/register', () => {
  const fillForm = () => {
    cy.get('input[type="text"]').type('bibi');
    cy.get('input[type="email"]').type('bibi@bibi.bi');
    cy.get('input[type="password"]').type('01234567890');
  };

  beforeEach(() => {
    cy.visitApp('/auth/register');
    cy.server();
  });

  it('should render correctly', () => {
    cy.get('input').should('have.length', 3);
    cy.get('button').should('exist')
  });

  it('should display error', () => {
    const getError = (index) => `index ${index}`;
    const errors = ['email', 'password', 'name', 'test'].reduce(
      (o, v, i) => ({ ...o, [v]: getError(i) }),
      {}
    );

    fillForm();

    cy.route('POST', API_URL_REGISTER, { errors });

    cy.get('button').click();

    cy.get('input.is-invalid').should('have.length', 3);
    cy.get('.invalid-feedback').should('have.length', 3);
    cy.get('ul.alert').should('exist');
  });

  it('on success redirect to login form', () => {
    fillForm();
    cy.server();
    cy.route('POST', API_URL_REGISTER, { success: true });

    cy.get('button').click();

    cy.url().should('include', URL_LOGIN);
  });
});
