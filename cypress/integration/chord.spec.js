describe('uku chord editor', () => {
  beforeEach(() => {
    cy.visit('/')
  });

  describe('default render', () => {
    it('has the correct <h1>', () => {
      cy.contains('h1', 'Ukulele chord generator')
    });

    it('has 4 chords', () => {
      cy.get('div.chord').should('lengthOf', 4);
    });

    it('image default url should be an classic Am7', () => {
      cy.get('main').find('img').should(($image) => {
        const src = $image[0].src;

        /* eslint-disable-next-line no-useless-escape */
        expect(src).to.match(/\?fingers=0,0,0,0\&name=Am7/);
      });
    });
  });

  describe('actions that should update image url', () => {
    it('name changed', () => {
      cy.get('input[id="chord-name"]').type('{selectall}{del}Dm7');

      cy.get('main').find('img').should(($image) => {
        expect($image[0].src).to.match(/name=Dm7/);
      });
    });

    it.only('chord changed', () => {
      cy.get('div.chord').first().find('input').first().type('{selectall}{del}3');

      cy.get('main').find('img').should(($image) => {
        expect($image[0].src).to.match(/fingers=3,0,0,0/);
      });
    });
  });
});
