describe('index', () => {
  beforeEach(() => {
    cy.visit('/')
  });

  describe('default render', () => {
    it('has the correct <h1>', () => {
      cy.contains('h1', 'Ukulele chord generator')
    });
  });
});
