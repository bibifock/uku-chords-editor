import { URL_LOGIN, API_URL_LOGOUT } from './auth/constant';

describe('me', () => {
  it('should redirect to login when not logged', () => {
    cy.visit('/me');
    cy.url().should('include', URL_LOGIN);
  });

  describe('when logged', () => {
    beforeEach(() => {
      cy.login();
      cy.get('a.nav-link').last().click();
    });

    it('should not redirect to login', () => {
      cy.url().should('include', '/me');
    });

    it('should full filled user infos', () => {
      cy.fixture('user').then(user => {
        cy.get('input').then(el => {
          cy.wrap(el[0]).should('have.value', user.email);
          cy.wrap(el[1]).should('have.value', user.name);
        })
      });
    });

    it.only('should logout should redirect to login page', () => {
      cy.server();
      cy.route('POST', API_URL_LOGOUT, { sucess: true });

      cy.get('button.btn-outline-danger').click().wait(50);

      cy.url().should('match', /^https?:\/\/[^/]+\/?$/);
    });
  });
});
