export const URL_TAB_LIST = '/tab';
export const URL_TAB_CREATE = '/tab/0';
export const URL_TAB_EDIT = '/tab/1';
export const API_URL_TAB_LIST = 'api/tabs.json';
export const API_URL_TAB_EDIT = 'api/tab/*.json';
