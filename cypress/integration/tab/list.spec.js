import { URL_TAB_LIST, URL_TAB_CREATE, API_URL_TAB_LIST } from './constant';

describe('tab/list', () => {
  beforeEach(() => {
    cy.server();
    cy.fixture('api/tabs').as('tabs').then(tabs => {
      cy.route(API_URL_TAB_LIST, tabs);
    });
  });

  describe('add tab link', () => {
    it('should be hide when user isn`t connect', () => {
      cy.visitApp(URL_TAB_LIST);
      cy.get('button').should('not.exist');
    });

    it('should redirect to new tab edit when user is connect', () => {
      cy.login();
      cy.goToNewTab();

      cy.url().should('include', URL_TAB_CREATE);
    });
  });

  /**
   * loaded by context modules so can't be mock
   * should use db test
   *
  it('should display all tabs', () => {
    cy.visitApp(URL_TAB_LIST);

    cy.get('@tabs').then(({ tabs }) => {
      cy.get('a.tab').should('have.length', tabs.length);
    });
  });

  it('tab click should open tab detail', () => {
    cy.visitApp(URL_TAB_LIST);

    cy.get('a.tab').first().click({ force: true }).wait(150);

    cy.url().should('match', /\/tab\/[1-9]+/);
  });
  */


  // it('should render correctly', () => {
  // // TODO voir pour les fixtures
  // // TODO chercke le requêtes qui partent
  // cy.get('a.tab').first().should('exist')
  // });

  // it('should show tab detail on hover', () => {
  // cy.get('a.tab').trigger('mouseenter');
  // cy.get('iframe').should('exist');
  // });
  // });
  // describe('creation form', () => {
  // beforeEach(() => {
  // cy.visit('/tabs/0');
  // });

  // it('should render correctly', () => {
  // cy.get('ul.errors').should('not.exist');
  // cy.get('input.error').should('not.exist');
  // });

  // it('should display error when form is uncomplete', () => {
  // cy.get('button').click();
  // cy.get('ul.errors').find('li').contains('missing value for fields');
  // cy.get('input.error').should('exist');
  // });
});
