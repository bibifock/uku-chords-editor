import {
  API_URL_TAB_EDIT,
  URL_TAB_CREATE,
  URL_TAB_EDIT,
  URL_TAB_LIST
} from './constant';

const tabListReg = new RegExp(URL_TAB_LIST + '$');

describe('tab/edit', () => {
  describe('unlogged', () => {
    it('in creation should redirect to tabs list', () => {
      cy.visitApp(URL_TAB_CREATE).wait(50);

      cy.url().should('match', tabListReg);
    });

    it('in edition sould display consult data', () => {
      cy.visitApp(URL_TAB_EDIT).wait(150);

      cy.get('input').should('not.exist');
      cy.get('button').should('not.exist');
    });
  });

  describe('logged', () => {
    beforeEach(() => {
      cy.login();
      cy.goToNewTab();

      cy.server();
      cy.fixture('api/categories').then(response => {
        cy.route('api/categories', response);
      })
    });

    it('should display creation form', () => {
      cy.get('input').should('exist');
      cy.get('button').should('exist');
    });

    it('should prefill author name', () => {
      cy.get('@loggedUser').then(({ name }) => {
        cy.get('input[name="author"]').should('have.value', name);
      });
    });

    it.only('creation should works', () => {
      cy.get('input[name="title"]').type('La thune');
      cy.get('input[name="artist"]').type('Angèle');
      cy.get('input[name="link"]').type('https://www.youtube.com/watch?v=m3YX8zlR4BU');

      cy.route('POST', API_URL_TAB_EDIT, { success: true, id: 9 });

      cy.get('button').click().wait(50);

      cy.url().should('match', tabListReg);

      cy.get('input[name="title"]').should('not.exist');
    });
  });
});
