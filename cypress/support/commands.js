import { URL_LOGIN, API_URL_LOGIN } from '../integration/auth/constant';
import { URL_TAB_LIST } from '../integration/tab/constant';

Cypress.Commands.add('login', () => {
  cy.visitApp(URL_LOGIN);

  cy.get('input[type="text"]').type('bibi');
  cy.get('input[type="password"]').type('01234');

  cy.fixture('user').as('loggedUser').then(user => {
    cy.server();
    cy.route(
      'POST',
      API_URL_LOGIN,
      { success: true, user }
    );

    cy.get('button').click().wait(50);
  });
});

Cypress.Commands.add('goToNewTab', () => {
  cy.get(`a.nav-link[href="${URL_TAB_LIST}"]`).click().wait(150);

  cy.url().should('include', URL_TAB_LIST);

  cy.get('button').click().wait(50);
});

Cypress.Commands.add('visitApp', (path, opts = {}) => {
  let polyfill;

  const polyfillUrl = 'https://unpkg.com/unfetch/dist/unfetch.umd.js'
  cy.request(polyfillUrl)
    .then((response) => {
      polyfill = response.body
    });

  cy.visit(
    path,
    Object.assign(opts, {
      onBeforeLoad (win) {
        delete win.fetch;
        // since the application code does not ship with a polyfill
        // load a polyfilled "fetch" from the test
        win.eval(polyfill)
        win.fetch = win.unfetch
      }
    })
  );
});
