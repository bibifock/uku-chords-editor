--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
CREATE TABLE tabs (
    title TEXT NOT NULL PRIMARY KEY COLLATE NOCASE,
    artist TEXT NULL COLLATE NOCASE,
    author TEXT NULL COLLATE NOCASE,
    link TEXT NULL COLLATE NOCASE,
    categoryId INTEGER NOT NULL,
    level INTEGER DEFAULT 1,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE categories (
    name TEXT NOT NULL PRIMARY KEY COLLATE NOCASE
);

INSERT INTO categories(name) values ('Chansons Internationales'), ('Chansons Françaises');

CREATE TABLE files (
    name TEXT NOT NULL COLLATE NOCASE,
    type TEXT NOT NULL COLLATE NOCASE,
    fullpath TEXT NOT NULL,
    idTab INTEGER NOT NULL,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (fullpath, idTab)
);
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
DROP TABLE tabs;

DROP TABLE categories;

DROP TABLE files;
