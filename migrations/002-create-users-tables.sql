--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
CREATE TABLE users (
    email TEXT NOT NULL PRIMARY KEY COLLATE NOCASE,
    name TEXT NULL COLLATE NOCASE,
    password TEXT NULL,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    lastConnection TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
DROP TABLE users;
