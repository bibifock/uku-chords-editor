--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
CREATE TABLE tabs_new (
    title TEXT NOT NULL COLLATE NOCASE,
    artist TEXT NULL COLLATE NOCASE,
    author TEXT NULL COLLATE NOCASE,
    link TEXT NULL COLLATE NOCASE,
    categoryId INTEGER NOT NULL,
    level INTEGER DEFAULT 1,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (title, artist)
);
INSERT INTO tabs_new (rowid, title, artist, author, categoryId, level, created, updated)
   SELECT rowid, title, artist, author, categoryId, level, created, updated FROM tabs;
DROP TABLE tabs;
ALTER TABLE tabs_new RENAME TO tabs;
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
