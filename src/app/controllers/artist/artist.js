import Artist from 'app/models/artist/Artist';

export const getAll = async ({ search, limit }) => {
  const artists = await Artist.all({ search, limit });

  return { artists };
};

export default {
  getAll
};
