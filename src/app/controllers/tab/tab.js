import File from 'app/models/file/File';
import Tab from 'app/models/tab/Tab';

import { haveError, getFilesToAdd } from './utils';

const getFullpath = ({ fullpath }) => fullpath;

const insertFilesForTab = async ({ tabId, files }) => {
  const filesToUpdate = getFilesToAdd(files, tabId);

  if (!filesToUpdate.length) {
    return true;
  }

  const filesToAdd = filesToUpdate.filter(({ id }) => !id);

  // check news files aren't already in database
  const fullpaths = filesToAdd.map(getFullpath);
  const results = await File.alreadyExists({ tabId, fullpaths });

  if (results) {
    return results.map(getFullpath);
  }

  await File.saveAll(filesToUpdate);

  return true;
}

export const getById = ({ params: { id }, session }) =>
  Tab.getById(id);

export const save = async ({ params: { id } = {}, body, session }) => {
  id = parseInt(id);
  const { files, ...tab } = body;

  const errors = haveError(tab, {});

  const result = await Tab.alreadyExists(tab);
  if (result && id !== result.id) {
    const { id: rowid, title } = result;
    errors.title = `${title} already present <a href="/tab/${rowid}" class="alert-link" target="_blank">here</a>`;
  }

  if (Object.keys(errors).length) {
    throw errors;
  }

  const { id: tabId } = await Tab.save(tab);

  insertFilesForTab({ tabId, files });

  return {
    success: true,
    id: tabId,
    tab: await Tab.getById(tabId)
  };
}

const orderByParam = {
  titleAsc: 'title ASC, artist ASC',
  titleDesc: 'title DESC, artist ASC',
  artistAsc: 'artist asc, title asc',
  artistDesc: 'artist desc, title asc',
  createdDesc: 'created desc, title ASC, artist ASC',
  updatedDesc: 'updated desc, title ASC, artist ASC'
};

export const getAll = async ({ search, sort, limit, withFiles }) => {
  const orderBy = orderByParam[sort];

  const tabs = await Tab.all({ search, orderBy, limit, withFiles });
  return { tabs };
};

export default {
  getById,
  getAll,
  save
};
