import makeCheckRequiredFields from 'utils/makeCheckRequiredFields';

/**
 * prepare files for insert in db
 * @param array file
 * @param ?int tabId
 *
 * @return func ( file )
 */
export const getFilesToAdd = (files, tabId) => {
  if (!files || !files.length) {
    return [];
  }

  return files
    .filter(({ src }) => src)
    .map(({ type, name, src, id, fullpath }) => ({
      // TODO remove space and special-chars
      fullpath: fullpath || `${name}_${tabId}.${type}`,
      id,
      type,
      name,
      src,
      tabId
    }));
};

const checkRequiredFields = makeCheckRequiredFields([
  'title', 'artist', 'categoryId'
]);

/**
 * @param object tab
 * @param ?any  successReturn
 *
 * @return boolean|object true when valid | { messages, fields }
 */
export const haveError = (tab, success = false) => {
  const errors = checkRequiredFields(tab);

  if (tab.link && !/https?:\/\//.test(tab.link)) {
    errors.link = 'should http link';
  }

  if (Object.keys(errors).length) {
    return errors
  }

  return success;
};
