import sqlite from 'sqlite';

const getDb = () => sqlite.open(process.env.DB_SOURCE);

export default getDb;
