import nPath from 'path';

import post from 'utils/post';
import uploadFile from 'utils/uploadFile';

import { haveError } from 'app/controllers/tab/utils';

import { API_URL_ARTISTS, API_URL_EDIT } from '../constants';

export async function searchArtists (search) {
  const { artists } = await post(API_URL_ARTISTS, { search, limit: 10 });
  return artists;
}

export async function uploadTabFile ({ files, file, index }) {
  const { file: { name, path } } = await uploadFile(file);

  const type = nPath.extname(name).substr(1);

  const newFile = {
    type,
    name: nPath.basename(name, '.' + type),
    src: path
  };

  const newFiles = [...files];

  if (index > -1) {
    newFiles[index] = { ...newFile, ...files[index] };
  } else {
    newFiles.push(newFile);
  }

  newFiles.sort((a, b) => a.name > b.name);
  return newFiles;
}

export async function saveTab (tab) {
  const { id = 0 } = tab;

  const errors = haveError(tab);
  if (errors) {
    return { errors };
  }

  const response = await post(
    API_URL_EDIT.replace('#ID#', id),
    tab
  );

  return response;
}

export function findFile ({ file }) {
  return ({ name, type }) => file.name === `${name}.${type}`;
}
