import jsonResponse from 'utils/jsonResponse';

import ArtistController from 'app/controllers/artist/artist';

export const post = jsonResponse(async (req, res, next) => {
  const { body } = req;
  const response = await ArtistController.getAll(body);

  return response;
});
