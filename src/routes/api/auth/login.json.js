import UserController from 'app/controllers/user/user';
import jsonResponse from 'utils/jsonResponse';

export const post = jsonResponse(async (req, res, next) => {
  const user = await UserController.login(req.body);

  /* eslint-disable-next-line require-atomic-updates */
  req.session.user = user;

  return { user, succes: true };
});
