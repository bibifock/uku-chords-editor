import UserController from 'app/controllers/user/user';
import jsonResponse from 'utils/jsonResponse';

export const post = jsonResponse(async (req, res, next) => {
  const id = await UserController.register(req.body);

  return { id, success: true };
});
