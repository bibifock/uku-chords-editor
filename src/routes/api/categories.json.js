import SQL from 'sql-template-strings';

import getDb from 'app/core/db';

export const get = async (req, res, next) => {
  const db = await getDb();

  const categories = await db.all(SQL`SELECT rowid, name FROM categories`);
  res.end(JSON.stringify({ categories }));

  await db.close();
}
