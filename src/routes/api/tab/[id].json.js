import TabController from 'app/controllers/tab/tab';
import jsonResponse from 'utils/jsonResponse';


export const post = jsonResponse(async (req, res, next) => {
  const response = await TabController.save(req);

  res.json(response);
});

export const get = jsonResponse(async (req, res, next) => {
  const { params } = req;
  const tab = await TabController.getById({ params });

  res.json({ tab });
});
