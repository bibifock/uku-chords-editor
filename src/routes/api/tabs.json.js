import jsonResponse from 'utils/jsonResponse';

import TabController from 'app/controllers/tab/tab';

export const post = jsonResponse(async (req, res, next) => {
  const { body } = req;
  const response = await TabController.getAll(body);

  return response;
});
