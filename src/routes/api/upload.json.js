import { Form } from 'multiparty';

const acceptedFileType = [
  'application/pdf',
  'application/vnd.oasis.opendocument.text',
  'application/msword',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
];

export function put (req, res, next) {
  const sendError = (code) => {
    res.writeHead(code, {
      'Content-Type': 'application/json'
    });

    res.end(JSON.stringify({
      success: false
    }));
  };

  if (!req.session.user) {
    return sendError(403);
  }

  const form = new Form();
  /* eslint-disable-next-line handle-callback-err */
  form.parse(req, (err, fields, files) => {
    const { file } = files;

    const { originalFilename: name, path, headers } = file[0];

    const contentType = headers['content-type'];
    if (acceptedFileType.indexOf(contentType) === -1 && !/^image\/.+$/.test(contentType)) {
      return sendError(400);
    }

    res.writeHead(200, {
      'Content-Type': 'application/json'
    });

    res.end(JSON.stringify({
      file: {
        name,
        path
      }
    }));
  });
}
