import sirv from 'sirv';
import polka from 'polka';
import compression from 'compression';
import bodyParser from 'body-parser';
import session from 'express-session';
import sessionFileStore from 'session-file-store';
import * as sapper from '@sapper/server';

const FileStore = sessionFileStore(session);

// const { PORT, NODE_ENV } = process.env;
// const dev = NODE_ENV === 'development';
const { PORT } = process.env;

import './i18n'

const app = polka() // You can also use Express
  .use(bodyParser.json())
  .use(session({
    secret: process.env.APP_SECRET,
    resave: false,
    saveUninitialized: true,
    cookie: {
      maxAge: 31536000
    },
    store: new FileStore({
      path: process.env.NOW ? '/tmp/sessions' : '.sessions'
    })
  }))
  .use(
    compression({ threshold: 0 }),
    sirv('static', { dev: true }),
    bodyParser.json(),
    sapper.middleware({
      session: req => ({
        user: req.session && req.session.user
      })
    })
  )
  .listen(PORT, err => {
    /* eslint-disable-next-line no-console */
    if (err) console.log('error', err);
  });

const handleExit = (signal) => {
  /* eslint-disable-next-line no-console */
  console.log(`Received ${signal}. Close my server properly.`)
  app.server.close(function () {
    process.exit(0);
  });
}

process.on('SIGINT', handleExit);
process.on('SIGQUIT', handleExit);
process.on('SIGTERM', handleExit);
