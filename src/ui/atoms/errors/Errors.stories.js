import Component from './Errors.svelte';

export default {
  title: 'atoms/errors'
};

export const base = () => ({
  Component,
  props: {
    errors: {
      'field-name': 'one error',
      system: 'and another <strong>one</strong>'
    }
  }
});
