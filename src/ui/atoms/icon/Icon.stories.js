import Component from './Icon.svelte';

const getStory = (props = {}) => ({
  Component,
  props: {
    name: 'contact',
    ...props
  }
});

export default {
  title: 'atoms/icon'
};

export const base = () => getStory();

export const medium = () => getStory({ medium: true });

export const large = () => getStory({ large: true });
