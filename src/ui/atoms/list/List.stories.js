import TabFactory from 'utils/testing/factory/Tab';

import ListTabItem from 'ui/organisms/list-tab-item/ListTabItem';

import Component from './List.svelte';

export default {
  title: 'atoms/list'
};

export const base = () => ({
  Component,
  props: {
    component: ListTabItem,
    items: [
      TabFactory(),
      TabFactory(),
      TabFactory(),
      TabFactory()
    ]
  }
});
