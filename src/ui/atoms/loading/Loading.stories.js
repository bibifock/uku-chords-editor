import Component from './Loading.svelte';

export default {
  title: 'atoms/loading'
};

export const base = () => ({
  Component
});
