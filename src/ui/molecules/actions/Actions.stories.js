import { generateActions } from 'utils/testing/storybook';

import Component from './Actions';

export default {
  title: 'molecules/actions'
}

export const base = () => ({
  Component,
  on: generateActions(['submit', 'cancel'])
});


export const saving = () => ({
  Component,
  props: {
    saving: true
  },
  on: generateActions(['submit', 'cancel'])
});
