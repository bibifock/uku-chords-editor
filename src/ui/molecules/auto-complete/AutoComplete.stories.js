import Component from './AutoComplete.svelte';

const on = {
  select: ({ detail }) => alert(JSON.stringify(detail))
};

const items = [0, 1, 2, 3].map(
  id => ({ id, label: `label ${id}`, value: `test ${id}` })
);

export default {
  title: 'molecules/auto-complete'
};

export const base = () => ({
  Component,
  props: {
    label: 'field label',
    placeholder: 'select a value',
    items
  },
  on
});

export const async = () => ({
  Component,
  props: {
    placeholder: 'select a value',
    loadOptions: (filter) => new Promise(
      (resolve) => resolve(
        items.filter(({ label }) => label.toLowerCase().includes(filter.toLowerCase()))
      )
    )
  },
  on
});

export const error = () => ({
  Component,
  props: {
    label: 'field label',
    placeholder: 'select a value',
    items,
    error: 'J’aime quand on m’enduit d’huile...'
  },
  on
});
