import { defaultProps as file } from 'ui/molecules/file/test.utils';
import Component from './ListFiles.svelte';

export default {
  title: 'molecules/list-files'
};

const { updated, created, ...newFile } = file;
newFile.fullpath = `test-${newFile.fullpath}`;

export const base = () => ({
  Component,
  props: { files: [file, file, newFile] }
});

export const errors = () => ({
  Component,
  props: {
    files: [file, file, newFile],
    errors: [
      newFile.fullpath
    ]
  }
});
