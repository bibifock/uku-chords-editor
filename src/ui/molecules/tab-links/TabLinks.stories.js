import faker from 'utils/testing/faker';

import Component from './TabLinks.svelte';

export default {
  title: 'molecules/tab-links'
};

const getStory = (props = {}) => ({
  Component,
  props: {
    youtube: 'https://www.youtube.com/watch?v=-G3MLjqicC8',
    file: faker.internet.url(),
    ...props
  }
});

export const base = () => getStory();

export const small = () => getStory({ small: true });
