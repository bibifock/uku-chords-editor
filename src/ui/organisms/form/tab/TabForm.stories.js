import { generateActions } from 'utils/testing/storybook';

import props from './test.utils';
import Component from './TabForm.svelte';

const on = generateActions([
  'submit',
  'cancel',
  'upload',
  'selectFile'
]);

export default {
  title: 'organisms/form/tab'
};

const fields = ['title', 'link', 'author', 'categoryId', 'level', 'artist'];

export const base = () => ({
  Component,
  props,
  on
});

export const errors = () => ({
  Component,
  props: {
    ...props,
    categoryId: 2,
    errors: fields.reduce(
      (o, key) => ({ ...o, [key]: 'this is an error' }),
      { system: 'Am I going crazy? Have my years of wild hedonism finally caught up with me?' }
    )
  },
  on
});
