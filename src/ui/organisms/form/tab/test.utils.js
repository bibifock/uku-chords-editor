import { defaultProps as filledProps } from 'ui/organisms/tab/test.utils';

const { file, ...props } = filledProps;

const tabs = [
  { title: 'Balance ton quoi', artist: 'Angèle', author: 'bbr', categoryId: 1 },
  { title: 'La thune', artist: 'Angèle', author: 'bbr', categoryId: 1 },
  { title: 'Onde sensuelle', artist: '-M-', author: 'rayah', categoryId: 1 },
  { title: 'Paint it black', artist: 'The Rolling Stones', author: 'bernie', categoryId: 2 }
].map((tab, rowid) => ({
  ...tab,
  rowid,
  link: `http://youtube.com/${tab.title}`,
  level: rowid
}));

const extractUniqKey = (list, key) => list.reduce(
  (arr, val) => {
    if (arr.indexOf(val[key]) === -1) {
      arr.push(val[key]);
    }
    return arr;
  },
  []
).map((val, rowid) => ({ [key]: val, rowid }));

const artists = extractUniqKey(tabs, 'artist');
const authors = extractUniqKey(tabs, 'author');

const filterIt = (filter, value) => value.toLowerCase().includes(filter.toLowerCase());

export const loadTabs = (filter) => new Promise(
  (resolve) => {
    setTimeout(
      () => resolve(tabs.filter(
        ({ title, artist }) => filterIt(filter, `${title} - ${artist}`)
      )),
      250
    )
  }
);

export const loadArtists = (filter) => new Promise(
  (resolve) => setTimeout(
    () => resolve(
      artists.filter(({ artist }) => filterIt(filter, artist))
    ),
    250
  )
);

export const loadAuthors = (filter) => new Promise(
  (resolve) => setTimeout(
    () => resolve(
      authors.filter(({ author }) => filterIt(filter, author))
    ),
    250
  )
);

export const categories = [
  { id: 1, label: 'Chanson française' },
  { id: 2, label: 'Chanson Internationale' }
];

export default {
  ...props,
  files: [file, { ...file, selected: true }],
  loadTabs,
  loadArtists,
  loadAuthors,
  categories
}
