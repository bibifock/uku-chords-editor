export const getOptionLabel = (key) => (o) => o[key];

export const getOptionTab = ({ title, artist }) => `${title} - ${artist}`;
