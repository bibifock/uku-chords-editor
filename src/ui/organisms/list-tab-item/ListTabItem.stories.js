import { defaultProps } from './test.utils';
import Component from './ListTabItem.svelte';

export default {
  title: 'organisms/list-tab-item'
};

const getStory = (props = {}) => ({
  Component,
  props: {
    ...defaultProps,
    ...props
  }
});

export const base = () => getStory();

export const compact = () => getStory({ compact: true });

export const noLink = () => getStory({ link: '' });
export const badLink = () => getStory({ link: 'http://perdu.com' });
