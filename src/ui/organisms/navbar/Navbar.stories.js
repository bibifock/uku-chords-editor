import Component from './Navbar.svelte';

export default {
  title: 'organisms/navbar'
};

export const base = () => ({
  Component,
  props: {
    page: '/',
    links: [
      { href: '/', label: 'Chord generator' },
      { href: '/tabs', label: 'Tabs' },
      { href: '/auth/login', label: 'sign in' },
      { href: '/me', label: 'bbr', icon: 'contact' }
    ]
  }
});
