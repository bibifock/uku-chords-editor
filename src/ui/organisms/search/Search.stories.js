import { generateActions } from 'utils/testing/storybook';

import Component from './Search.svelte';

export default {
  title: 'organisms/search'
};

const on = generateActions(['search', 'sort']);

const sorts = [
  { label: 'tab.sorts.titleAsc', value: 'titleAsc' },
  { label: 'tab.sorts.titleDesc', value: 'titleDesc' }
];

export const base = () => ({
  Component,
  props: {
    placeholder: 'search ...',
    caption: '10 tabs founds',
    sort: sorts[0],
    sorts
  },
  on
});
