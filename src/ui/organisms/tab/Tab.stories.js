import { defaultProps as props } from './test.utils';
import Component from './Tab.svelte';

export default {
  title: 'organisms/tab'
};

export const base = () => ({
  Component,
  props,
  on: {
    click: ({ detail }) => alert(JSON.stringify(detail))
  }
});

export const detail = () => ({
  Component,
  props: {
    ...props,
    open: true
  }
});

export const noLink = () => ({
  Component,
  props: {
    ...props,
    link: ''
  }
});

export const badLink = () => ({
  Component,
  props: {
    ...props,
    link: 'http://perdu.com'
  }
});
