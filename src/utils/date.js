export const parseDate = (value) => {
  const regDate = /^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$/;

  if (!regDate.test(value)) {
    return false;
  }

  const result = value.match(regDate);

  return new Date(
    parseInt(result[1]),
    parseInt(result[2]) - 1,
    parseInt(result[3]),
    parseInt(result[4]),
    parseInt(result[5]),
    parseInt(result[6])
  );
};
